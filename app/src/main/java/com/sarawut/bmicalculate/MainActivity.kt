package com.sarawut.bmicalculate

import android.content.Context
import android.icu.text.NumberFormat
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.sarawut.bmicalculate.databinding.ActivityMainBinding
import java.math.RoundingMode
import java.text.DecimalFormat

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.calculateButton.setOnClickListener { calculateBMI() }
        binding.userHeight.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view, keyCode)}
        binding.userHeight.setOnKeyListener{ view, keyCode, _ -> handleKeyEvent(view, keyCode)}
    }

    private fun calculateBMI() {
        val weight = binding.userWeight.text.toString().toDoubleOrNull()
        val height = binding.userHeight.text.toString().toDoubleOrNull()
        if ((weight == null || height == null) || (weight == 0.0 || height == 0.0)) {
            alert("Invalid input! Please enter input again.")
            showResult(0.0)
            return
        }
        val heightMeter = height / 100
        val userBMIResult = weight / Math.pow(heightMeter, 2.0)

        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.DOWN
        val userBMIResultFormatted = df.format(userBMIResult).toDouble()

        val userBMILevel = levelCalculate(userBMIResultFormatted)
        alert("$userBMIResultFormatted | $userBMILevel")
        showResult(userBMIResultFormatted)
    }

    private fun showResult(userBMI: Double) {
        val userBMILevel = levelCalculate(userBMI)

        binding.userBmiResult.text = getString(R.string.bmi_result, userBMI.toString())
        binding.userBmiLevel.text = getString(R.string.bmi_level_result, userBMILevel)
    }

    private fun levelCalculate(bmi: Double): String {
        if (bmi <= 0) {
            alert("Invalid input! Please enter input again.")
            return "NO RESULT"
        } else if (bmi < 18.5) {
            return "UNDERWEIGHT"
        } else if (bmi < 25) {
            return "NORMAL"
        } else if (bmi <= 30) {
            return "OVERWEIGHT"
        } else {
            return "OBESE"
        }
    }

    private fun alert(textShow: String) {
        val toast = Toast.makeText(this, textShow, Toast.LENGTH_SHORT)
        toast.show()
    }

    private fun handleKeyEvent(view: View, keyCode: Int): Boolean {
        if(keyCode == KeyEvent.KEYCODE_ENTER){
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
            return true
        }else{
            return false
        }
    }
}